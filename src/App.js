import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Parent from './parent.js';
import Second from './second.js'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Second/>
        <Parent/>
      </div>
    );
  }
}

export default App;
