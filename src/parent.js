import React from 'react';
import Firstchild from './firstchild.js';
import Second from './second.js';


class Parent extends React.Component {
   constructor(props) {
      super(props);
       this.state = {
         name :"",
         age : "",
         mobile: ""
           }
      this.updateState = this.updateState.bind(this);
       };
          
      updateState(event) {
        this.setState(
          {
           [event.target.name] : event.target.value
           
          }
        );
       }
       
render() {
  return (
         <div>
             <Firstchild dataUpdate={this.updateState}/>
             <Second name={this.state.name} age={this.state.age} mobile={this.state.mobile}/>

            </div>
          )
   }
}
export default Parent;