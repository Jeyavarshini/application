import React from 'react';
class Firstchild extends React.Component {
    constructor (props) { 
    super(props);
    this.displayData = this.displayData.bind(this);
    }
    displayData(event)
     {
        this.props.dataUpdate(event);
     }
    render () {
       return (
           <div>
            <input type = "text" name='name' 
            onChange = {this.displayData} />
            <input type = "text" name='age' 
            onChange = {this.displayData} />
            <input type = "text" name='mobile' 
            onChange = {this.displayData} />
            </div>
    )

    }

}           
    
export default Firstchild;